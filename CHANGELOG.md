# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2024-01-17
### Changed
-  #1 build sumo image from dds basis image
-  #1 use newest SUMO version

## [1.0.0] - 2024-01-15
### Added
- First release
