FROM reg.osmhpi.de/flexidug/cyclonedds/3.10-slim-bookworm:1.0.1

RUN apt update
RUN apt upgrade -y

# Build SUMO from Source
RUN apt install -y git cmake python3 g++ libxerces-c-dev libfox-1.6-dev libgdal-dev libproj-dev libgl2ps-dev python3-dev swig default-jdk maven libeigen3-dev

# SUMO version 1.19.0
RUN git clone --recursive --depth 1 --branch v1_19_0 https://github.com/eclipse-sumo/sumo

WORKDIR /app/sumo
RUN cmake -B build .
RUN cmake --build build -j$(nproc)
WORKDIR /app

# Set environment variables
ENV SUMO_HOME="/app/sumo"
ENV PATH="$SUMO_HOME/bin:$PATH"
ENV PYTHONPATH="$SUMO_HOME/tools/:$PYTHONPATH"
# For sumo-gui
ENV DISPLAY=host.docker.internal:0
