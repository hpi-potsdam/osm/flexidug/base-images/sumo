# SUMO Docker Image

To build this image, clone this repository and run:
```
docker build -t sumo .
```

To start a container from this image run:
```
docker run -dit --name sumo sumo
```

Now you can start `sumo` with `docker exec` or run a python script using `import traci`.

# Run SUMO GUI on macOS

Before creating the container, run
```
xhost +
```
on the macOS.

Afterwards, when starting the container, add the following parameters:
```
-v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=host.docker.internal:0
```

# Troubleshooting

## Authorization required

If you are facing this issue:
```
Authorization required, but no authorization protocol specified

FXApp::openDisplay: unable to open display host.docker.internal:0
```

Run:
```
xhost +
```
on the host machine.

## X Error

If you are facing this issue:
```
X Error: code 2 major 149 minor 3: BadValue (integer parameter out of range for operation).
```

Try this:
```
defaults write org.xquartz.X11 enable_iglx -bool YES
```
on the host machine.